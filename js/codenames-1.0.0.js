var colors = ['grey','beige','red','blue','green'];
var redBegin = [1,7,9,8];
var blueBegin = [1,7,8,9];
var duetConversion = [[0,4],[1,4],[4,4],[4,1],[4,0],[1,0],[0,0],[1,1],[0,1]];
var duet = [1,5,3,5,1,1,1,7,1];
var arrays = [];
var mode = 0;
var changeArray = [];
changeArray[0] = ['white','red','blue','beige','grey','green','white'];
changeArray[1] = ['white','red','blue','beige','grey','white'];
changeArray[2] = ['white','green','beige','grey','white'];

function generateWords(){
    var nums = [];
    var i = 0;
    do {
        var index = chance.integer({ min: 0, max: wordsCs.length - 1 })
        if (nums.indexOf(index) === -1){
            nums.push(index);
            i++;
        }
    } while (i < 25);
    shuffle(nums);
    drawWords(nums);
}

function drawWords(nums) {
    for (var i = 0; i < nums.length; i++){
        var row = Math.floor(i / 5) + 1;
        var col = i % 5 + 1;
        $('#target tr:nth-child(' + row + ') td:nth-child('+col+') span').html(wordsCs[nums[i]]);
    }
    $('#wordsCode').val(createCode(wordsCs, nums));
}

function changeColor(tdTag){
    var clazz = tdTag.attr('class');
    if (clazz === undefined) clazz = 'white';
    var index = changeArray[mode].indexOf(clazz);
    var nextIndex = index + 1;
    tdTag.attr('class', changeArray[mode][nextIndex]);
}

function strike(td) {
    var tdTag = $(td);
    tdTag.toggleClass('strike');
}

function loadWords(){
    var strLength = wordsCs.length.toString().length; // number of ciphers
    var code = $('#wordsCode').val();
    var nums = [];
    for (var i = 0; i < code.length / strLength; i++){
        var num = parseInt(code.substring(i * 3, (i+1) * 3), 10);
        nums.push(num);
    }
    drawWords(nums);
}

// creates simple code to loading same situation
function createCode(words, results){
    var strLength = words.length.toString().length; // number of ciphers
    var longWord = '';
    for (var i = 0; i < results.length; i++){
        longWord += results[i].toString().padStart(strLength, '0'); // uh, ugly
    }
    return longWord;
}

function deleteWords(){
    $('td').empty();
}
function deleteColors() {
    $('td, table').attr('class','');
    mode = 0;
}

function generateCommon(){
    var array = generateArray(Math.random() > 0.5 ? redBegin : blueBegin);
    arrays = [array];
    var encoded = encodeArray(array, 5);
    $('#commonCode').val(encoded);
    $('#code1').val(encoded);
    $('#code2').val('');
    drawCode(0)
}
function loadCommon() {
    var encoded = $('#commonCode').val();
    var array = decodeArray(encoded);
    arrays = [array];
    $('#code1').val(encoded);
    $('#code2').val('');
    drawCode(0);
}
function generateDuet(){
    var array = generateArray(duet);
    $('#duetCode').val(encodeArray(array,5));
    assignSideArrays(array);
    $('#code1').val(encodeArray(arrays[0], 5));
    $('#code2').val(encodeArray(arrays[1], 5));
}
function loadDuet(){
    var array = decodeArray($('#duetCode').val());
    assignSideArrays(array);
    $('#code1').val(encodeArray(arrays[0], 5));
    $('#code2').val(encodeArray(arrays[1], 5));
}
function drawCode(index){
    var code = $('#code' + (index+1)).val();
    if (code === '') return
    draw(decodeArray(code));
}
function assignSideArrays(array) {
    var first = [];
    var second = [];
    for (var i = 0; i < array.length; i++){
        first.push(duetConversion[array[i]][0]);
        second.push(duetConversion[array[i]][1]);
    }
    arrays = [first, second];
}
function draw(array) {
    var reds = 0;
    var blues = 0;
    for (var i = 0; i < array.length; i++){
        var col = i % 5 + 1;
        var row = Math.floor(i / 5) + 1;
        var css = colors[array[i]];
        if (array[i] === 2){
            reds++;
        } else if (array[i] === 3){
            blues++;
        }
        $('#target tr:nth-child(' + row + ') td:nth-child('+col+')').attr('class',css);
    }
    var tableBorderClass;
    if (reds === 0 && blues === 0){
        tableBorderClass = '';
        mode = 2;
    } else {
        var red = reds > blues;
        if (red) {
            tableBorderClass = 'red';
        } else {
            tableBorderClass = 'blue';
        }
        mode = 1;
    }
    $('#target').attr('class', tableBorderClass);
}
function generateArray(distrArr){
    var arr = [];
    for (var i = 0; i < distrArr.length; i++){
        for (var j = 0; j < distrArr[i]; j++){
            arr.push(i);
        }
    }
    shuffle(arr);
    return arr;
}
/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

$(document).ready(function () {
    for (var i = 0; i < 5; i++){
        $('#target').append('<tr></tr>');
    }
    for (var i = 0; i < 5; i++) {
        $('#target tr').append('<td><a class="uk-icon-button strikebtn" uk-icon="strikethrough"></a> <span class="wordplace"></span> <a class="uk-icon-button paintbtn" uk-icon="paint-bucket"></a></td>');
    }
    $('.paintbtn').on('click', function () {
        changeColor($(this).parent());
    })
    $('.strikebtn').on('click', function () {
        strike($(this).parent());
    })
})