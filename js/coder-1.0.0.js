var symbols = 'crtx48hbkv7zlqn5ewf1g63udm0p9ijo2ysa';
var base = symbols.length;
var separator = '.';

function encodeNumber(sourceNumber) {
    var powers = [];
    var e = 0;
    var powValue;
    do {
        powValue = Math.pow(base, e);
        powers.push(powValue);
        e++;
    } while(powValue < sourceNumber);
    powers.reverse();
    powers.shift(); // removes last (greater) value
    var tempResult = sourceNumber;
    var code = '';
    for (var i = 0; i < powers.length; i++){
        var c = Math.floor(tempResult / powers[i]);
        code += symbols.charAt(c);
        tempResult -= c * powers[i];
    }
    return code;
}
function encodeArray(array, splitCount){
    var parts = splitParts(array, splitCount);
    var code = '';
    for (var i = 0; i < parts.length; i++){
        code += encodeNumber(parts[i]) + separator;
    }
    return code.substring(0,code.length - 1);
}
function splitParts(array, splitCount){
    var numbers = [];
    var numStr = '';
    for (var i = 0; i < array.length; i++){
        if (i % splitCount === 0){
            if (numStr.length > 0)
                numbers.push(parseInt(numStr, 10));
            numStr = '';
        }
        numStr += array[i];
    }
    numbers.push(parseInt(numStr, 10));
    return numbers;
}
function decodeNumber(code){
    var singleSymbols = code.split('');
    var result = 0;
    for (var i = 0; i < singleSymbols.length; i++){
        var e = singleSymbols.length - i - 1;
        var multiplier = symbols.indexOf(singleSymbols[i]);
        result += multiplier * Math.pow(base, e);
    }
    return result;
}
function decodeArray(text) {
    var str = '';
    var encParts = text.split(separator);
    for (var i = 0; i < encParts.length; i++){
        str += decodeNumber(encParts[i]).toString().padStart(5, '0');
    }
    var splitNums = str.split('');
    var array = [];
    for (var i = 0; i < splitNums.length; i++){
        array.push(parseInt(splitNums[i],10));
    }
    return array;
}