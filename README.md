# Codenames generators
Generators for online playing of great word party game [Codenames](https://boardgamegeek.com/boardgame/178900/codenames) of Vlaada Chvátil. Runs on [http://deskos.cz/codenames](http://deskos.cz/codenames). 

You can use it for generate card of classic [Codenames](https://boardgamegeek.com/boardgame/178900/codenames) or [cooperative Duet](https://boardgamegeek.com/boardgame/224037/codenames-duet) and pass it by code to another player. Now in Czech language, but it isn't problem I think.

![screenshot](http://deskos.cz/codenames/img/screenshot.png) 